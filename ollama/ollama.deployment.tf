variable "properties" {
  type = any
}

variable "dependency" {
  type = any
}

variable "project" {
  type = any
}

locals {
  service_name = var.properties.local_name
  cluster      = var.dependency.cloudfairy_cluster
  env_name     = var.project.environment_name
  is_dev       = substr(var.project.environment_name, 0, 3) == "dev"
}

data "google_client_config" "gcp" {
  count = local.is_dev ? 0 : 1
}

provider "kubernetes" {
  insecure               = local.is_dev
  config_path            = local.is_dev ? local.cluster.kubeconfig_path : null
  cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
  token                  = local.is_dev ? null : data.google_client_config.gcp[0].access_token
  host                   = local.is_dev ? null : "https://${local.cluster.hostname}"
}

provider "helm" {
  kubernetes {
    insecure               = local.is_dev
    config_path            = local.is_dev ? local.cluster.kubeconfig_path : null
    cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
    token                  = local.is_dev ? null : data.google_client_config.gcp[0].access_token
    host                   = local.is_dev ? null : "https://${local.cluster.hostname}"
  }
}

resource "helm_release" "ollama" {
  wait          = false
  wait_for_jobs = false
  name          = "${local.service_name}-ollama"
  chart         = "https://github.com/otwld/ollama-helm/releases/download/ollama-0.29.1/ollama-0.29.1.tgz"

  set {
    name  = "ollama.gpu.enabled"
    value = "false"
  }

  set {
    name  = "ollama.fullName"
    value = "${local.service_name}-ollama"
  }

  set {
    name  = "nameOverride"
    value = local.service_name
  }

  set_list {
    name  = "ollama.models"
    value = [var.properties.model]
  }

  set {
    name  = "persistentVolume.enabled"
    value = "true"
  }

  set {
    name  = "persistentVolume.existingClaim"
    value = local.is_dev ? "data-volume-claim" : "${local.service_name}-pvc-1"
  }

  set {
    name  = "persistentVolume.accessModes"
    value = "[\"ReadWriteMany\"]"
  }

  # set {
  #   name = "ingress.enabled"
  #   value = var.properties.has_ingress == "1" ? "true" : "false"
  #   type = "auto"
  # }

  # set {
  #   name = "ingress.hosts[0].host"
  #   value = var.properties.has_ingress == "1" ? var.dependency.cloudfairy_cluster.hostname : null
  # }
}

resource "kubernetes_storage_class_v1" "cloud_storage" {
  count = local.is_dev ? 0 : 1
  metadata {
    name = "${local.service_name}-storage-class"
    annotations = {
      "cloudfairy.dev/managed" : "true"
    }
  }
  storage_provisioner    = "kubernetes.io/gce-pd"
  reclaim_policy         = "Delete"
  allow_volume_expansion = true
  volume_binding_mode    = "Immediate"
  parameters = {
    type = "pd-ssd"
  }
}

resource "kubernetes_persistent_volume_claim_v1" "cloud_storage" {
  wait_until_bound = false
  count            = local.is_dev ? 0 : 1
  metadata {
    name = "${local.service_name}-pvc-1"
  }
  spec {
    storage_class_name = "${local.service_name}-storage-class"
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        "storage" = "30Gi"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim_v1" "cloud_storage_web" {
  wait_until_bound = false
  count            = local.is_dev ? 0 : 1
  metadata {
    name = "${local.service_name}-pvc-2"
  }
  spec {
    storage_class_name = "${local.service_name}-storage-class"
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        "storage" = "30Gi"
      }
    }
  }
}

resource "kubernetes_ingress_v1" "ollama" {
  count = var.properties.has_ingress
  metadata {
    name = local.service_name
    labels = {
      "app" = local.service_name
    }
    namespace = "default"
    annotations = {
      "cert-manager.io/cluster-issuer"                   = "letsencrypt-prod"
      "kubernetes.io/ingress.class"                      = local.is_dev ? "traefik" : "gce"
      "traefik.ingress.kubernetes.io/router.entrypoints" = "web,websecure"
    }
  }
  spec {
    ingress_class_name = local.is_dev ? "traefik" : "gce"
    rule {
      host = local.is_dev ? "${local.service_name}.localhost" : null
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "${local.service_name}-ollama"
              port {
                number = 11434
              }
            }
          }
        }
      }
    }
  }
}


########## WEB UI

resource "kubernetes_deployment_v1" "webui" {
  wait_for_rollout = false
  metadata {
    name = "${local.service_name}-ollama-webui"
    labels = {
      app = "${local.service_name}-ollama-webui"
    }
  }
  spec {
    selector {
      match_labels = {
        app = "${local.service_name}-ollama-webui"
      }
    }
    template {
      metadata {
        labels = {
          "app" = "${local.service_name}-ollama-webui"
        }
      }
      spec {
        container {
          name  = "${local.service_name}-ollama-webui"
          image = "ghcr.io/open-webui/open-webui:main"
          port {
            container_port = 8080
          }
          resources {
            limits = {
              cpu    = "1000m"
              memory = "1Gi"
            }
            requests = {
              cpu    = "500m"
              memory = "1Gi"
            }
          }
          env {
            name  = "OLLAMA_BASE_URL"
            value = "http://${local.service_name}-ollama:11434"
          }
          tty = true
          volume_mount {
            name       = "${local.service_name}-volumemount"
            mount_path = "/app/backend/data"
            sub_path   = "${local.service_name}/"
          }
        }
        volume {
          name = "${local.service_name}-volumemount"
          persistent_volume_claim {
            claim_name = local.is_dev ? "data-volume-claim" : "${local.service_name}-pvc-2"
          }
        }

      }
    }
  }
}

resource "kubernetes_service_v1" "webui" {
  metadata {
    name = "${local.service_name}-ollama-webui"
    labels = {
      app = "${local.service_name}-ollama-webui"
    }
  }
  spec {
    type = "NodePort"
    selector = {
      app = "${local.service_name}-ollama-webui"
    }
    port {
      port        = 10686
      target_port = 8080
    }
  }
}

resource "kubernetes_ingress_v1" "webui" {
  count = var.properties.has_ingress
  metadata {
    name = "${local.service_name}-ollama-webui"
    labels = {
      app = "${local.service_name}-ollama-webui"
    }
    namespace = "default"
    annotations = {
      "cert-manager.io/cluster-issuer"                   = "letsencrypt-prod"
      "kubernetes.io/ingress.class"                      = local.is_dev ? "traefik" : "gce"
      "traefik.ingress.kubernetes.io/router.entrypoints" = "web,websecure"
    }
  }
  spec {
    ingress_class_name = local.is_dev ? "traefik" : "gce"
    rule {
      host = local.is_dev ? "${local.service_name}-webui.localhost" : null
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "${local.service_name}-ollama-webui"
              port {
                number = 10686
              }
            }
          }
        }
      }
    }
  }

}

########## END OF WEB UI

output "cfout" {
  value = {
    service_name  = "${local.service_name}-ollama-api"
    hostname      = local.service_name
    port          = 11434
    web_ui_port   = 10686
    documentation = <<EOF
# ${local.service_name} Ollama OpenAPI server + UI
Internal kubernetes access:
- API: ${local.service_name}-ollama:11434
- WebUI: ${local.service_name}-ollama-webui:10686
Access with port-forwarding:
```bash
kubectl port-forward ${local.service_name}-ollama 10686:10686
kubectl port-forward ${local.service_name}-ollama-api 11434:11434
```
EOF
  }
}
