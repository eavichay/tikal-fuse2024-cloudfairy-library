variable "properties" {
  type = any
}

variable "dependency" {
  type = any
}

variable "project" {
  type = any
}

locals {
  kname = var.properties.local_name
  cluster = var.dependency.cloudfairy_cluster
  env_name = var.project.environment_name
  is_dev = substr(var.project.environment_name, 0, 3) == "dev"
  service_name = "${local.kname}-kafka-broker"
  advertise_listeners = local.is_dev ? "advertisedListeners: \"PLAINTEXT://localhost:9092\"" : ""
}

data "google_client_config" "gcp" {
  count = local.is_dev ? 0 : 1
}

provider "helm" {
  kubernetes {
    insecure = local.is_dev
    config_path = local.is_dev ? local.cluster.kubeconfig_path : null
    cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
    token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
    host = local.is_dev ? null : "https://${local.cluster.hostname}"
  }

  registry {
    url = "oci://registry-1.docker.io/bitnamicharts/kafka"
    username = ""
    password = ""
  }
}

provider "kubernetes" {
  insecure = local.is_dev
  config_path = local.is_dev ? local.cluster.kubeconfig_path : null
  cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
  token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
  host = local.is_dev ? null : "https://${local.cluster.hostname}"
}

resource "kubernetes_deployment_v1" "zookeeper" {
  metadata {
    name = "${local.kname}-zookeeper"
    labels = {
      app = "${local.kname}-zookeeper"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "${local.kname}-zookeeper"
      }
    }
    template {
      metadata {
        labels = {
          app = "${local.kname}-zookeeper"
        }
      }
      spec {
        container {
          name = "${local.kname}-zookeeper"
          image = "bitnami/zookeeper"
          port {
            container_port = 2181
          }
          env {
            name = "ALLOW_ANONYMOUS_LOGIN"
            value = "yes"
          }
        }
      }
    }
  }
  wait_for_rollout = false
}

resource "kubernetes_service_v1" "zookeeper" {
  metadata {
    name = "${local.kname}-zookeeper"
    labels = {
      app = "${local.kname}-zookeeper"
    }
  }
  spec {
    type = "NodePort"
    selector = {
      app = "${local.kname}-zookeeper"
    }
    port {
      name = "zookeeper-port"
      port = 2181
      node_port = 30181
      target_port = 2181
    }
  }
}

resource "kubernetes_deployment_v1" "kafkabroker" {
  metadata {
    name = local.service_name
    labels = {
      app = local.service_name
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = local.service_name
      }
    }
    template {
      metadata {
        labels = {
          app = local.service_name
        }
      }
      spec {
        hostname = local.service_name
        container {
          name = local.service_name
          image = "bitnami/kafka"
          image_pull_policy = "Always"
          port {
            container_port = 9092
          }
          env {
            name = "KAFKA_BROKER_ID"
            value = "1"
          }
          env {
            name = "KAFKA_ZOOKEEPER_CONNECT"
            value = "${local.kname}-zookeeper:2181"
          }
          env {
            name = "KAFKA_LISTENERS"
            value = "PLAINTEXT://:9092"
          }
          env {
            name = "KAFKA_ADVERTISED_LISTENERS"
            value = "PLAINTEXT://localhost:9092"
          }
        }
      }
    }
  }
  wait_for_rollout = false
}

resource "kubernetes_service_v1" "kafkabroker" {
  metadata {
    name = local.service_name
    labels = {
      app = local.service_name
    }
  }
  spec {
    type = "NodePort"
    selector = {
      app = local.service_name
    }
    port {
      name = "kafka-port"
      port = 9092
    }
  
  }
}


output "cfout" {
  value = {
    service_name = local.service_name
    hostname = local.service_name
    port = 9092
    documentation =<<EOF

# ${local.kname} kafka broker (+zookeeper)
On local environment, the connection is PLAINTEXT
External access available only with port-forward:
```bash
kubectl port-forward service/${local.service_name} 9092:9092
```

EOF
  }
}