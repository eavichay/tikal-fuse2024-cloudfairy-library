variable "properties" {
  type = any
}

variable "dependency" {
  type = any
}

variable "project" {
  type = any
}

locals {
  service_name = var.properties.local_name
  cluster = var.dependency.cloudfairy_cluster
  env_name = var.project.environment_name
  is_dev = substr(var.project.environment_name, 0, 3) == "dev"
}

data "google_client_config" "gcp" {
  count = local.is_dev ? 0 : 1
}

provider "helm" {
  kubernetes {
    insecure = local.is_dev
    config_path = local.is_dev ? local.cluster.kubeconfig_path : null
    cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
    token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
    host = local.is_dev ? null : "https://${local.cluster.hostname}"
  }
}

provider "kubernetes" {
  insecure = local.is_dev
  config_path = local.is_dev ? local.cluster.kubeconfig_path : null
  cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
  token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
  host = local.is_dev ? null : "https://${local.cluster.hostname}"
}

resource "helm_release" "milvus" {
  wait = false
  wait_for_jobs = false
  name = "${local.service_name}-milvus"
  chart = "https://github.com/milvus-io/milvus-helm/releases/download/milvus-4.0.31/milvus-4.0.31.tgz"

  set {
    name = "cluster.enabled"
    value = "false"
    type = "auto"
  }
  
  set {
    name = "pulsar.enabled"
    value = "false"
    type = "auto"
  }
  
  # set {
  #   name = "minio.mode"
  #   value = "standalone"
  #   type = "auto"
  # }
  
  set {
    name = "etcd.replicaCount"
    value = 3
    type = "auto"
  }
}

output "cfout" {
  value = {
    service_name = "${local.service_name}-milvus"
    port = 19530
    documentation = <<EOF
# ${local.service_name} MilvusDB
Kubernetes internal access: ${local.service_name}-milvus:19530

Access via port-forward:
```bash
kubectl port-forward svc/${local.service_name}-milvus 19530:19530
```
EOF
  }
}