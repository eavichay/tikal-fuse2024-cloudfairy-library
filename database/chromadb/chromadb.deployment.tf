variable "properties" {
  # service_name
  # repo_url
  type = any
}

variable "project" {
  # environment_name
  type = any
}

variable "dependency" {
  # cloud_provider
  # cluster
  type = any
}

locals {
  has_ingress = try(var.properties.has_ingress, false)
  service_name = var.properties.local_name
  port = 8000
  cluster = var.dependency.cloudfairy_cluster
  env_name = var.project.environment_name
  is_dev = substr(var.project.environment_name, 0, 3) == "dev"
}

data "google_client_config" "gcp" {
  count = local.is_dev ? 0 : 1
}

provider "helm" {
  kubernetes {
    insecure = local.is_dev
    config_path = local.is_dev ? local.cluster.kubeconfig_path : null
    cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
    token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
    host = local.is_dev ? null : "https://${local.cluster.hostname}"
  }
}

provider "kubernetes" {
  insecure = local.is_dev
  config_path = local.is_dev ? local.cluster.kubeconfig_path : null
  cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
  token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
  host = local.is_dev ? null : "https://${local.cluster.hostname}"
}

resource "random_string" "auth_token" {
  length = 32
  special = false
}

resource "helm_release" "chromadb" {
  wait = false
  wait_for_jobs = false
  name       = "${local.service_name}-chromadb"
  chart      = "https://github.com/amikos-tech/chromadb-chart/releases/download/chromadb-0.1.19/chromadb-0.1.19.tgz"

  set {
    name  = "ingress.enabled"
    value = "false"
  }
  
  set {
    name = "chromadb.auth.token.value"
    value = random_string.auth_token.result
  }

}

output "cfout" {
  sensitive = false
  value = {
    hostname = "${local.service_name}-chromadb"
    service_name = "${local.service_name}-chromadb"
    port = local.port
    token = random_string.auth_token.result
    documentation = <<EOF
# ${local.service_name} ChromaDB
Connect via port-forwarding:
```bash
kubectl port-forward svc/${local.service_name}-chromadb ${local.port}:${local.port}
```
On your *local* environment only, the access token is ${random_string.auth_token.result}
EOF
  } 
}