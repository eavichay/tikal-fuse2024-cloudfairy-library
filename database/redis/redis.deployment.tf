variable "properties" {
  type = any
}

variable "dependency" {
  type = any
}

variable "project" {
  type = any
}

locals {
  service_name = var.properties.local_name
  cluster = var.dependency.cloudfairy_cluster
  env_name = var.project.environment_name
  is_dev = substr(var.project.environment_name, 0, 3) == "dev"
}

data "google_client_config" "gcp" {
  count = local.is_dev ? 0 : 1
}

provider "helm" {
  kubernetes {
    insecure = local.is_dev
    config_path = local.is_dev ? local.cluster.kubeconfig_path : null
    cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
    token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
    host = local.is_dev ? null : "https://${local.cluster.hostname}"
  }
}

provider "kubernetes" {
  insecure = local.is_dev
  config_path = local.is_dev ? local.cluster.kubeconfig_path : null
  cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
  token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
  host = local.is_dev ? null : "https://${local.cluster.hostname}"
}

resource "random_string" "redis_password" {
  length = local.is_dev ? 4 : 32
  special = local.is_dev ? false : true
}

resource "helm_release" "redis" {
  wait = false
  wait_for_jobs = false
  name = "${local.service_name}-redis"
  chart = "oci://registry-1.docker.io/bitnamicharts/redis"
  version = "19.3.0"

  set {
    name = "cluster.enabled"
    value = local.is_dev ? "false" : var.properties.num_replicas > 0 ? "true" : "false"
    type = "auto"
  }

  set {
    name = "master.persistence.enabled"
    value = var.properties.has_persistent_disk ? "true" : "false"
    type = "auto"
  }
  set {
    name = "replica.persistence.enabled"
    value = var.properties.has_persistent_disk ? "true" : "false"
    type = "auto"
  }
  set {
    name = "replica.replicaCount"
    value = local.is_dev ? 0 : var.properties.num_replicas
  }
  set {
    name = "auth.enabled"
    value = var.properties.has_auth
    type = "auto"
  }
  set {
    name = "auth.password"
    value = var.properties.has_auth ? random_string.redis_password.result : ""
    type = "auto"
  }
}

output "cfout" {
  value = {
    service_name = "${local.service_name}-redis-master"
    hostname = "${local.service_name}-redis-master"
    db_user = ""
    db_pass = base64encode(random_string.redis_password.result)
    port = 6379 
    documentation = <<EOF
# ${local.service_name} Redis Database
Kubernetes internal access: ${local.service_name}-redis:6379 

> ### Please note:
>
> User and password auto-generated, injectable via environment variables to connected services.
>
> On local environeemnt only, the password is a 4-character string.
>
> Clustering is not supported on local environment.
Access via port-forward:
```bash
kubectl port-forward svc/${local.service_name}-milvus 6379:6379 
```
EOF
  }
}