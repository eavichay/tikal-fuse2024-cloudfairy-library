variable "properties" {
  type = any
}

variable "dependency" {
  type = any
}

variable "project" {
  type = any
}

locals {
  has_ingress = try(var.properties.has_ingress, false)
  service_name = var.properties.local_name
  port = 6379
  cluster = var.dependency.cloudfairy_cluster
  env_name = var.project.environment_name
  is_dev = substr(var.project.environment_name, 0, 3) == "dev"
}


data "google_client_config" "gcp" {
  count = local.is_dev ? 0 : 1
}

provider "helm" {
  kubernetes {
    insecure = local.is_dev
    config_path = local.is_dev ? local.cluster.kubeconfig_path : null
    cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
    token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
    host = local.is_dev ? null : "https://${local.cluster.hostname}"
  }
}

provider "kubernetes" {
  insecure = local.is_dev
  config_path = local.is_dev ? local.cluster.kubeconfig_path : null
  cluster_ca_certificate = local.is_dev ? null : base64decode(local.cluster.cluster_ca_certificate)
  token = local.is_dev ? null : data.google_client_config.gcp[0].access_token
  host = local.is_dev ? null : "https://${local.cluster.hostname}"
}



resource "kubernetes_deployment_v1" "falkordb" {
  wait_for_rollout = false
  metadata {
    name = local.service_name
    namespace = "default"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        "app" = local.service_name
      }
    }
    template {
      metadata {
        labels = {
          "app" = local.service_name 
        }
      }
      spec {
        service_account_name = var.dependency.cloudfairy_cluster.service_account
        container {
          name = local.service_name
          image = "falkordb/falkordb:latest"
          image_pull_policy = "Always"
          port {
            container_port = local.port
            protocol = "TCP"
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "service" {
  metadata {
    name = local.service_name
    namespace = "default"
    labels = {
      "app" = local.service_name
    }
  }
  spec {
    type = "ClusterIP"
    selector = {
      "app" = local.service_name
    }
    port {
      port = local.port
      target_port = local.port
      protocol = "TCP"
    }
  }
}

resource "kubernetes_ingress_v1" "ingress" {
  count = local.has_ingress ? 1 : 0
  metadata {
    name = local.service_name
    labels = {
      "app" = local.service_name
    }
    annotations = {
      "kubernetes.io/ingress.class" = "traefik"
      "traefik.ingress.kubernetes.io/router.entrypoints" = "web,websecure"
    }
  }
  spec {
    rule {
      host = local.is_dev ? "${local.service_name}.localhost" : "*"
      http {
        path {
          path = "/"
          path_type = "Prefix"
          backend {
            service {
              name = local.service_name
              port {
                number = local.port
              }
            }
          }
        }
      }
    }
  }
}

output "cfout" {
  value = {
    service_name = local.service_name
    port = local.port
    documentation = <<EOF
# ${local.service_name} FalkorDB
Connect via port-forwarding:
```bash
kubectl port-forward svc/${local.service_name} ${local.port}:${local.port}
```
EOF
  }
}